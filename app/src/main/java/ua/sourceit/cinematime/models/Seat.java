package ua.sourceit.cinematime.models;

import java.io.Serializable;

public class Seat implements Serializable {
    private long id;
    private int num;
    private int row;
    private Hall hall;
    private Type type;

    public Seat(long id, int num, int row, Hall hall, Type type) {
        this.id = id;
        this.num = num;
        this.row = row;
        this.hall = hall;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
