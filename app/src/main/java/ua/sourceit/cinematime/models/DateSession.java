package ua.sourceit.cinematime.models;

import java.io.Serializable;
import java.util.List;

public class DateSession implements Serializable {
    private String date;
    private List<Session> sessions;

    public DateSession(String date, List<Session> sessions) {
        this.date = date;
        this.sessions = sessions;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }


}
