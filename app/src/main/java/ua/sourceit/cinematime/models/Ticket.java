package ua.sourceit.cinematime.models;

import java.io.Serializable;

public class Ticket implements Serializable {
    private long id;
    private Session session;
    private Seat seat;
    private Order order;

    public Ticket(long id, Session session, Seat seat, Order order) {
        this.id = id;
        this.session = session;
        this.seat = seat;
        this.order = order;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
