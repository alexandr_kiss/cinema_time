package ua.sourceit.cinematime.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Hall implements Serializable {
    private long id;
    private ArrayList<Seat> seats;

    public Hall(long id, ArrayList<Seat> seats) {
        this.id = id;
        this.seats = seats;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Seat> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
}
