package ua.sourceit.cinematime.models;


import java.io.Serializable;
import java.util.ArrayList;

public class Movie implements Serializable {
    private long id;
    private String name;
    private String genre;
    private int duration;
    private String img;
    private String description;
    private String trailer;
    private ArrayList<DateSession> dateSessions;

    //for tests
    public Movie(String name, int duration, String img, String description) {
        this.name = name;
        this.duration = duration;
        this.img = img;
        this.description = description;
    }

    public Movie(long id, String name, String genre, int duration, String img, String description, String trailer, ArrayList<DateSession> dateSessions) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.duration = duration;
        this.img = img;
        this.description = description;
        this.trailer = trailer;
        this.dateSessions = dateSessions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String  getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrailer() {
        return trailer.split("=")[1];
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public ArrayList<DateSession> getDateSessions() {
        return dateSessions;
    }

    public void setDateSessions(ArrayList<DateSession> dateSessions) {
        this.dateSessions = dateSessions;
    }

    public String getDurationText() {
        int hour = duration/60;
        int minutes = duration - (hour*60);
        return hour + " год. " + minutes + " хв.";
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", duration=" + duration +
                ", img=" + img +
                ", description='" + description + '\'' +
                ", trailer=" + trailer +
                '}';
    }
}
