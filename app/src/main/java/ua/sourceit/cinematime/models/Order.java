package ua.sourceit.cinematime.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable {
    private long id;
    private ArrayList<Ticket> ticket;

    public Order(long id, ArrayList<Ticket> ticket) {
        this.id = id;
        this.ticket = ticket;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(ArrayList<Ticket> ticket) {
        this.ticket = ticket;
    }
}
