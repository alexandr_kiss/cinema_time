package ua.sourceit.cinematime.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Session implements Serializable {
    private long id;
    private String time;
    private Movie movie;
    private Hall hall;
    private ArrayList<Ticket> tickets;

    public Session(long id, String time, Movie movie, Hall hall, ArrayList<Ticket> tickets) {
        this.id = id;
        this.time = time;
        this.movie = movie;
        this.hall = hall;
        this.tickets = tickets;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", time='" + time + '\'' +
                '}';
    }
}
