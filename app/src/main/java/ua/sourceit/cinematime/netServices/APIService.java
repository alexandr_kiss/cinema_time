package ua.sourceit.cinematime.netServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import ua.sourceit.cinematime.models.Movie;

public interface APIService {
    @GET("posts")
    Call<List<Movie>> loadMovie();
}
