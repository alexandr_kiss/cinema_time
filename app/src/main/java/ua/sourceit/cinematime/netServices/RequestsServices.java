package ua.sourceit.cinematime.netServices;

import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.sourceit.cinematime.models.Movie;
import ua.sourceit.cinematime.ui.MovieListAdapter;

public class RequestsServices {
    public static void getListMovies(final RecyclerView list) {
        NetworkService.getInstance()
                .getJSONApi()
                .loadMovie()
                .enqueue(new Callback<List<Movie>>() {
                    @Override
                    public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                        MovieListAdapter adapter = new MovieListAdapter(response.body());
                        list.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<List<Movie>> call, Throwable t) {
                        Log.e("Retrofit error","Error occurred while getting request!");
                        t.printStackTrace();
                    }
                });
    }
}
