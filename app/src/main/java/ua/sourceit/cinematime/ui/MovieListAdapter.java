package ua.sourceit.cinematime.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ua.sourceit.cinematime.R;
import ua.sourceit.cinematime.models.Movie;
import ua.sourceit.cinematime.models.Session;

public class MovieListAdapter extends RecyclerView.Adapter<MovieHolder> {
    private List<Movie> movieList;
    private Context context;

    public MovieListAdapter(List<Movie> movieList) {
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View benchItemView = ((Activity) context).getLayoutInflater().inflate(R.layout.movie_list_item, parent, false);
        return new MovieHolder(benchItemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Movie movie = movieList.get(position);
        List<Session> sessions = movie.getDateSessions().get(0).getSessions();
        StringBuilder timeSession = new StringBuilder();

        for (int i = 0; i < sessions.size(); i++) {
            Session session = sessions.get(i);
            timeSession.append(session.getTime());
            if (i < sessions.size()-1) timeSession.append(", ");
        }

        if (position == (movieList.size()-1))
            holder.getItem().setPadding(0,0,0,100);

        holder.getName().setText(movie.getName());
        holder.getDuration().setText(timeSession);
        holder.getDescription().setText(movie.getDescription());
        Picasso.get().load((movie.getImg())).into(holder.getPoster());

        holder.getBuyTicket().setOnClickListener(v -> {
            Intent intent = new Intent(context, MovieAboutActivity.class);
            intent.putExtra("movie", movie);
            context.startActivity(intent);
            Log.i("onClick", movie.getName());
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
