package ua.sourceit.cinematime.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ua.sourceit.cinematime.R;

public class MovieAboutSessionTimeHolder extends RecyclerView.ViewHolder {
    private TextView time;

    TextView getTime() {
        return time;
    }

    public MovieAboutSessionTimeHolder(@NonNull final View itemView) {
        super(itemView);
        this.time = itemView.findViewById(R.id.session_time);
    }
}
