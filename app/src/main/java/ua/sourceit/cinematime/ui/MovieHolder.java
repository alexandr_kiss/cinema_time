package ua.sourceit.cinematime.ui;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ua.sourceit.cinematime.R;

class MovieHolder extends RecyclerView.ViewHolder {
    private FrameLayout item;
    private TextView name;
    private TextView duration;
    private ImageView poster;
    private TextView description;
    private TextView buyTicket;

    public FrameLayout getItem() {
        return item;
    }

    TextView getName() {
        return name;
    }

    TextView getDuration() {
        return duration;
    }

    ImageView getPoster() {
        return poster;
    }

    TextView getDescription() {
        return description;
    }

    TextView getBuyTicket() {
        return buyTicket;
    }

    @SuppressLint("CutPasteId")
    MovieHolder(@NonNull final View itemView) {
        super(itemView);
        item = itemView.findViewById(R.id.movie_list_item);
        name = itemView.findViewById(R.id.title);
        duration = itemView.findViewById(R.id.duration);
        poster = itemView.findViewById(R.id.poster);
        description = itemView.findViewById(R.id.description);
        buyTicket = itemView.findViewById(R.id.buyTicket);
    }
}
