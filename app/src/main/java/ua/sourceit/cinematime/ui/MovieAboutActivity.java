package ua.sourceit.cinematime.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import jp.wasabeef.blurry.Blurry;
import ua.sourceit.cinematime.R;
import ua.sourceit.cinematime.models.Movie;

public class MovieAboutActivity extends AppCompatActivity {
    ImageView poster;
    YouTubePlayerView trailer;
    TextView title;
    TextView genre;
    TextView duration;
    TextView description;
    RecyclerView sessions;

    LinearLayout allTextAboutMovie;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.CustomBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_the_movie_activity);

        poster = findViewById(R.id.poster_about);
        trailer = findViewById(R.id.youtube_trailer_about);
        title = findViewById(R.id.title_about);
        genre = findViewById(R.id.genre_about);
        duration = findViewById(R.id.duration_about);
        description = findViewById(R.id.description_about);
        sessions = findViewById(R.id.sessions_about);
        allTextAboutMovie = findViewById(R.id.all_text_about);

        Bundle arguments = getIntent().getExtras();
        Movie movie;
        if(arguments!=null){
            movie = (Movie) arguments.getSerializable("movie");

            if (movie != null) {
                Picasso.get().load(movie.getImg()).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Blurry.with(MovieAboutActivity.this)
                                .radius(25)
                                .color(Color.argb(66, 43, 43, 43))
                                .from(bitmap)
                                .into(poster);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {}

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });


                getLifecycle().addObserver(trailer);
                trailer.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                        youTubePlayer.cueVideo(movie.getTrailer(), 0);
                        allTextAboutMovie.startAnimation(AnimationUtils.loadAnimation(MovieAboutActivity.this, R.anim.load_video));
                        trailer.setVisibility(View.VISIBLE);
                    }
                });

                title.setText(movie.getName());
                genre.setText(movie.getGenre());
                duration.setText(movie.getDurationText());
                description.setText(movie.getDescription());

                LinearLayoutManager verticalLayoutManager
                        = new LinearLayoutManager(MovieAboutActivity.this, LinearLayoutManager.VERTICAL, false);
                sessions.setLayoutManager(verticalLayoutManager);
                MovieAboutSessionsAdapter adapter = new MovieAboutSessionsAdapter(movie.getDateSessions());
                sessions.setAdapter(adapter);
            }
        }
    }
}