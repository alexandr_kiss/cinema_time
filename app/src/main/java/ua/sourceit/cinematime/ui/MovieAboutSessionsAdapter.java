package ua.sourceit.cinematime.ui;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ua.sourceit.cinematime.R;
import ua.sourceit.cinematime.models.DateSession;

public class MovieAboutSessionsAdapter extends RecyclerView.Adapter<MovieAboutSessionHolder> {
    private List<DateSession> sessions;
    private Context context;

    public MovieAboutSessionsAdapter(List<DateSession> sessions) {
        this.sessions = sessions;
    }

    @NonNull
    @Override
    public MovieAboutSessionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        View benchItemView = ((Activity) context).getLayoutInflater().inflate(R.layout.about_session_list_item, parent, false);
        return new MovieAboutSessionHolder(benchItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAboutSessionHolder holder, int position) {
        final DateSession dateSession = sessions.get(position);
        holder.getDate().setText(dateSession.getDate());

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.getTime().setLayoutManager(horizontalLayoutManager);
        MovieAboutSessionsTimeAdapter adapter = new MovieAboutSessionsTimeAdapter(dateSession.getSessions());
        holder.getTime().setAdapter(adapter);
    }

    @Override
        public int getItemCount() {
        return sessions.size();
    }
}
