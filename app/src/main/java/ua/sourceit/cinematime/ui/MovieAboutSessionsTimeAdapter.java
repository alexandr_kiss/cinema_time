package ua.sourceit.cinematime.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ua.sourceit.cinematime.R;
import ua.sourceit.cinematime.models.Session;

public class MovieAboutSessionsTimeAdapter extends RecyclerView.Adapter<MovieAboutSessionTimeHolder> {
    private List<Session> sessions;

    public MovieAboutSessionsTimeAdapter(List<Session> sessions) {
        this.sessions = sessions;
    }

    @NonNull
    @Override
    public MovieAboutSessionTimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View benchItemView = ((Activity) context).getLayoutInflater().inflate(R.layout.about_session_list_item_time, parent, false);
        return new MovieAboutSessionTimeHolder(benchItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAboutSessionTimeHolder holder, int position) {
        final Session session = sessions.get(position);
        holder.getTime().setPaintFlags(holder.getTime().getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.getTime().setText(session.getTime());
    }

    @Override
    public int getItemCount() {
        return sessions.size();
    }
}
