package ua.sourceit.cinematime.ui;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import ua.sourceit.cinematime.R;
import ua.sourceit.cinematime.netServices.RequestsServices;

public class MovieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.CustomBar);
        getWindow().setNavigationBarColor(Color.parseColor("#801F1F1F"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_list);

        RequestsServices.getListMovies(findViewById(R.id.list));
    }
}