package ua.sourceit.cinematime.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ua.sourceit.cinematime.R;

public class MovieAboutSessionHolder extends RecyclerView.ViewHolder {
    private TextView date;
    private RecyclerView time;

    public TextView getDate() {
        return date;
    }

    public RecyclerView getTime() {
        return time;
    }

    public MovieAboutSessionHolder(@NonNull final View itemView) {
        super(itemView);
        this.date = itemView.findViewById(R.id.session_date);
        this.time = itemView.findViewById(R.id.session_times);
    }
}
