# Cinema Time

#### Глобальные цели проекта
1. Создать Android приложение для заказа билетов в кино, которое будет взаимодействовать с API сервера.
2. Создать простой сервер с REST API для хранения информации о фильмах, сеансах, забронированных билетах, кинозалах и расположению мест в кинозале.

#### Описание Android приложения
##### Приложение должно состоять из 6 activity:
 - **Splash Activity** - тема с логотипом кинотеатра, которая будет показана при загрузке приложения, вместо черного экрана.
 - **List Movies Activity** - список фильмов, которые сейчас доступны для просмотра в кинотеатре. Реализовать с помощью ArrayAdapter. Информацию получаем с сервера списком.
 - **About the Movie Activity** - информация о фильме и доступных сеансах. Информацию берем из ранее загруженных данных с сервера. Данные о сеансах подгружаем дополнительно с сервера.
 - **Ticket Booking Activity** - бронирование билетов в кинозале. Информацию о расположении мест берем с сервера в формате JSON или XML (предпочтительно). Так же подгружаем данные о занятых местах. Возможность выбрать несколько мест для броинрования.
 - **Order Payment Activity** - внесение платежных данных для оплаты. Так как биллинг подключать не будем, то activity не обязательно.
 - **Booked Tickets Activity** - отображение забронированных билетов в одном заказе. Так же генерируеться QR код с информацией о номере бронирования. Возможность загрузить билет в формат PDF.

![Alt text](Cinema_Time_min.jpg "Navigation Activity")

#### Описание сервера
(описание будет позже)

#### Общие модели для приложения и сервера
| Class | Fields |
| ----- | ------ |
| Movie | `long` **id**, `String` **name**, `String` **genre**, `int` **duration**, `Uri` **trailer**, `ArrayList<Session>` **sessions** *(OneToMany)* |
| Session | `long` **id**, `Date` **date**, `Movie` **movie** *(ManyToOne)*, `Hal` *hall*, `ArrayList<Ticket>` **tickets** *(OneToMany)* |
|Hall |`long` **id**, `ArrayList<Seat>` **seats** *(OneToMany)*|
|Seat| `long` **id**, `int` **num**, `int` **row**, `Hall` **hall** *(ManyToOne)*, `Type` **type**|
|Type| `long` **id**, `String` **name** *(эконом, стандарт, премиум)*, `double` **price** |
|Ticket| `long` **id**, `Session` **session**, `Seat` **seat**, `Order` **order** *(ManyToOne)*|
|Order| `long` **id**, `ArrayList<Ticket>` **ticket** *(OneToMany)*|

`TODO` - *необходимо придумать, как хранить информацию о координатах Seat для Hall. Необходимо для возможности подключать дополнительные залы без hard code.*

#### Задачи (Android App)
- [x] Описать все модели.
- [x] Сделать тему `Splash Activity`. Добавить ее в *drawable* и добавить в манифест как основную тему
    - [x] нарисовать logo.
- [x] Создать layout `List Movies Activity`
     - [x] создать layout-заготовку пункта меню;
     - [x] загрузка списка фильмов с API.
- [ ]  Создать layout `About the Movie Activity`
    - [ ] создать layout-заготовку сеанса.
- [ ]  Создать layout `Order Payment Activity` *(не обязательно)*.
- [ ]  Создать layout `Booked Tickets Activity`.
- [ ]  Создать layout `Ticket Booking Activity` без зала, но оставить место для него. 

Список задач будет дополняться
